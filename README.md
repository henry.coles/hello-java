# Hello java

## Things to do

### Check me out 

If you can read this you've already done that. Hurrah!

### Run the tests

```shell script
./gradlew test
```

Lots should fail, don't worry about that yet

### Create a branch

```shell script
git checkout -b your_name
```

### Import me into Intelij

Click click click

### Now start to make the tests run green

One at a time.

After each test is green commit.

```shell script
git status
git add .
git commit -m 'your message'
```

Then push

```shell script
git push
```



