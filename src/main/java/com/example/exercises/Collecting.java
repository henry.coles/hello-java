package com.example.exercises;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Collecting {

    public Set<String> loop(Collection<String> things) {
        // put things into a Set using a loop
       return Collections.emptySet();
    }

    public Set<String> collect(Collection<String> things) {
        // put things into a Set using streams
        return Collections.emptySet();
    }

    public Set<String> collectIterable(Iterable<String> things) {
        // put things into a Set using streams. Iterable does not
        // have a stream method - boo
        return Collections.emptySet();
    }

    public List<String> collectArray(String[] things) {
        // put things into a List using streams. Arrays do not
        // have a stream method - boo
        return Collections.emptyList();
    }
    public String join(Collection<String> things) {
        // join things with commas
        return "";
    }

    public String joinVarArgs(String ... strings) {
        // join string with commas. Var args are just arrays
        return "";
    }
}
