package com.example.exercises;

import java.io.IOException;
import java.io.PrintWriter;

public class Iterating {

    public void forEachLoop(Iterable<String> things, PrintWriter writer) throws IOException {
        // writer.write all the things using a for each loop
    }

    public void forEach(Iterable<String> things, PrintWriter writer) throws IOException {
        // writer.write all the things using the forEach method
    }

    public interface Thing {
        void call(Iterable<String> things, PrintWriter writer);
    }

}
