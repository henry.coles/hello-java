package com.example.exercises;

import java.util.List;
import java.util.Set;

public class SmallAndEmptyThings {

    public static List<String> anEmptyList() {
        // return a empty list using new
        return null;
    }

    public static List<String> anotherEmptyList() {
        // return an empty list using a utility method from Collections
        return null;
    }

    public static List<String> yetAnotherEmptyList() {
        // return an empty list using a utility method from List
        return null;
    }

    public static Set<Integer> aSetWithOneEntry() {
        // return a set with one entry using new
        return null;
    }

    public static Set<Integer> anotherSetWithOneEntry() {
        // return a set with one entry using a utility method on Set
        return null;
    }
}
