package com.example.exercises;

import java.util.Optional;
import java.util.OptionalInt;

// make the tests pass
public class Optionals {
    Optional<String> alwaysEmpty() {
        return null;
    }

    String defaultsToFoo(Optional<String> maybeString) {
        return "";
    }

    Optional<String> prependHelloIfPresent(Optional<String> maybeString) {
        return maybeString;
    }

    OptionalInt multiplyByFourIfPresent(OptionalInt maybeInt) {
        return maybeInt;
    }

}