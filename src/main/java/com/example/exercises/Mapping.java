package com.example.exercises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Mapping {

    public List<Integer> mapLoop(List<Integer> is) {
        // multiply integers > 2 by 4 using a loop
        return Collections.emptyList();
    }

    public List<Integer> map(List<Integer> is) {
        // multiply integers > 2 by 4 using streams
        return Collections.emptyList();
    }

    public List<String> mapBuzz(Integer ... is) {
        // implement fizzBuzz using the fizzBuzzMethod
        return Collections.emptyList();
    }

    private Stream<String> fizzBuzz(Integer i) {
        if (i%3 == 0 && i%5 == 0) {
            return Stream.of("Fizz", "Buzz");
        }

        if (i%3 == 0) {
            return Stream.of("Fizz");
        }

        if (i%5 == 0) {
            return Stream.of("Buzz");
        }

        return Stream.of(""+i);
    }
}
