package com.example.exercises;

import java.util.function.Function;
import java.util.function.IntFunction;

public class Lambdas {

    public static Function<String, Integer> lambda() {
        // call Integer.valueOf as an lambda
        return null;
    }

    public static Function<String, Integer> methodReference() {
        // call Integer.valueOf as a method reference
        return null;
    }

    public static IntFunction<String> primitiveLambda() {
        // call String.valueOf as a lambda
        return null;
    }

    public static IntFunction<String> primitiveMethodRef() {
        // call String.valueOf as a method reference
        return null;
    }
}
