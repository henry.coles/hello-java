package com.example.exercises;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class D_MappingTest {
    Mapping underTest = new Mapping();

    @Test
    public void mapLoopIsImplemented() {
      assertThat(underTest.mapLoop(List.of(1,2,3,4,5)))
        .containsExactly(12, 16, 20);
    }

    @Test
    public void mapIsImplemented() {
        assertThat(underTest.mapLoop(List.of(1,2,3,4,5)))
                .containsExactly(12, 16, 20);
    }

    @Test
    public void fizzBuzzIsImplemented() {
        assertThat(underTest.mapBuzz(1,2,3,4,5,6,10,11,12))
                .containsExactly("1", "2","Fizz", "4", "Buzz", "Fizz", "Buzz", "11", "Fizz");
    }
}