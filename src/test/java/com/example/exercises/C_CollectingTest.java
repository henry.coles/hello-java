package com.example.exercises;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class C_CollectingTest {

    Collecting underTest = new Collecting();

    @Test
    public void loopIsImplemented() {
        assertThat(underTest.loop(List.of("a", "b", "c", "c")))
                .containsExactly("a", "b", "c");
    }

    @Test
    public void collectIsImplemented() {
        assertThat(underTest.collect(List.of("a", "b", "c", "c")))
                .containsExactly("a", "b", "c");
    }

    @Test
    public void collectArrayIsImplemented() {
        assertThat(underTest.collectArray(new String[]{"a", "b", "c"}))
                .containsExactly("a", "b", "c");
    }

    @Test
    public void collectIterableIsImplemented() {
        assertThat(underTest.collectIterable(List.of("a", "b", "c", "c")))
                .containsExactly("a", "b", "c");
    }

    @Test
    public void joinIsImplemented() {
        assertThat(underTest.join(List.of("1", "2", "3")))
                .isEqualTo("1,2,3");
    }

    @Test
    public void joinVarArgsIsImplemented() {
        assertThat(underTest.joinVarArgs("1", "2", "3"))
                .isEqualTo("1,2,3");
    }
}