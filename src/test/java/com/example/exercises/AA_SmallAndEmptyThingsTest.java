package com.example.exercises;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AA_SmallAndEmptyThingsTest {

    @Test
    public void anEmptyListIsImplemented() {
        assertThat(SmallAndEmptyThings.anEmptyList())
                .isNotNull()
                .isEmpty();
    }

    @Test
    public void anotherEmptyListIsImplemented() {
        assertThat(SmallAndEmptyThings.anotherEmptyList())
                .isNotNull()
                .isEmpty();
    }

    @Test
    public void yetAnotherEmptyListIsImplemented() {
        assertThat(SmallAndEmptyThings.yetAnotherEmptyList())
                .isNotNull()
                .isEmpty();
    }

    @Test
    public void aSetWithOneEntryIsImplemented() {
        assertThat(SmallAndEmptyThings.aSetWithOneEntry())
                .isNotNull()
                .hasSize(1);
    }

    @Test
    public void anotherSetWithOneEntryIsImplemented() {
        assertThat(SmallAndEmptyThings.aSetWithOneEntry())
                .isNotNull()
                .hasSize(1);
    }
}