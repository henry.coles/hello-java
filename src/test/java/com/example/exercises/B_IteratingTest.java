package com.example.exercises;

import org.junit.Test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class B_IteratingTest {

    Iterating underTest = new Iterating();
    Writer writer = new StringWriter();

    @Test
    public void forEachLoopIsImplemented() throws Exception {
        underTest.forEachLoop(List.of("a", "b", "c"), new PrintWriter(writer));
        assertThat(writer.toString()).isEqualTo("abc");
    }

    @Test
    public void forEachLoopIsd() throws Exception {
        underTest.forEach(List.of("a", "b", "c"), new PrintWriter(writer));
        assertThat(writer.toString()).isEqualTo("abc");
    }

}