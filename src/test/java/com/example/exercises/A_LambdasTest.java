package com.example.exercises;

import org.junit.Test;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;

import static org.assertj.core.api.Assertions.assertThat;

public class A_LambdasTest {

    @Test
    public void lambdaIsImplemented() {
        assertThat(Lambdas.lambda())
                .isNotNull()
                .satisfies(parsesInts());
    }

    @Test
    public void methodRefIsImplemented() {
        assertThat(Lambdas.methodReference())
                .isNotNull()
                .satisfies(parsesInts());
    }

    @Test
    public void primitiveLambdaIsImplemented() {
        assertThat(Lambdas.primitiveLambda())
                .isNotNull()
                .satisfies(parsesStrings());
    }

    @Test
    public void primitiveMethodRefIsImplemented() {
        assertThat(Lambdas.primitiveMethodRef())
                .isNotNull()
                .satisfies(parsesStrings());
    }

    private Consumer<Function<String, Integer>> parsesInts() {
        return f -> assertThat(f.apply("42")).isEqualTo(42);
    }

    private Consumer<IntFunction<String>> parsesStrings() {
        return f -> assertThat(f.apply(42)).isEqualTo("42");

    }


}