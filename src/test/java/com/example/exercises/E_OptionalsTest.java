package com.example.exercises;

import org.junit.Test;

import java.util.Optional;
import java.util.OptionalInt;

import static org.assertj.core.api.Assertions.assertThat;

public class E_OptionalsTest {

    Optionals underTest = new Optionals();

    @Test
    public void alwaysEmptyIsImplemented() {
        assertThat(underTest.alwaysEmpty())
                .isNotNull()
                .isEmpty();
    }

    @Test
    public void defaultToFooIsImplemented() {
        assertThat(underTest.defaultsToFoo(Optional.of("Bar")))
                .isEqualTo("Bar");
        assertThat(underTest.defaultsToFoo(Optional.empty()))
                .isEqualTo("Foo");
    }

    @Test
    public void prependHelloIfPresentIsImplemented() {
        assertThat(underTest.prependHelloIfPresent(Optional.of("Bar")))
                .contains("HelloBar");
        assertThat(underTest.prependHelloIfPresent(Optional.empty()))
                .isEmpty();
    }

    @Test
    public void multiplyByFourIfPresentIsImplemented() {
        assertThat(underTest.multiplyByFourIfPresent(OptionalInt.empty()))
                 .isEmpty();

        assertThat(underTest.multiplyByFourIfPresent(OptionalInt.of(2)))
                .hasValue(8);

        assertThat(underTest.multiplyByFourIfPresent(OptionalInt.of(4)))
                .hasValue(16);
    }

}